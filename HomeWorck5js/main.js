// 1) Метод об'єкта це функція яка належить об'єкту.
// Конструктор Object створює об'єкт-обгортку для переданого значення.
// Якщо значенням є null або undefined, створює і повертає порожній об'єкт,
// в іншому випадку повертає об'єкт такого типу, який відповідає переданому значенням.
// 2)Властивість є значення або набір значень (у вигляді масиву або об'єкта),
// який є членом об'єкта і може містити будь який тип даних.
// 3)Типи посилань — це об'єкти, включаючи Object, і Function.
// Зважаючи на те, що ці типи можуть містити дуже великі обсяги дуже різнорідних даних, змінна,
//  що містить тип посилань, фактично його значення не містить.
//   Вона містить посилання місце у пам'яті, де розміщуються реальні дані.

console.log(creatNewUser());

function creatNewUser() {
  const newUser = {
    _firstName: prompt("Enter your name"),
    _lastName: prompt("Enter your last name"),

    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },

    setFirstName(value) {
      this._firstName = value;
    },
    setLastName(value) {
      this._lastName = value;
    },
  };
  newUser.setFirstName("Nikola");
  newUser.setLastName("Tesla");
  console.log(newUser.getLogin());
  return newUser;
}



