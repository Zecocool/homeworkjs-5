// 1) const element = document.createElement(tagName);
// element — створений об'єкт елемента
// tagName — рядок, який вказує на елемент якого типу має бути створений. 
// nodeName створюється та ініціалізується зі значенням tagName. 
// 2)insertAdjacentHTML() разбирает указанный текст как HTML и вставляет полученные узлы (nodes) в DOM дерево в указанную позицию.
// пример :  targetElement.insertAdjacentHTML(position, text);
// 'beforebegin': до самого element (до открывающего тега).
// 'afterbegin': сразу после открывающего тега element (перед первым потомком).
// 'beforeend': сразу перед закрывающим тегом element (после последнего потомка).
// 'afterend': после element (после закрывающего тега).
// 3)Метод Element.remove() видаляє елемент із DOM-дерева, в якому він знаходиться.

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["Kharkiv", "Kiev", ["Borispol",["Borispol", "Irpin"], "Irpin"], "Odessa", "Lviv", "Dnieper"];
function arrayToList(arr, parent = document.body){
    const ul = document.createElement("ul");
    parent.append(ul)
    if(Array.isArray(arr)){
        arr.forEach((element)=> {
            if(Array.isArray(element)) {
                arrayToList(element, ul)
            }else{

                const li =document.createElement("li")
                ul.append(li)
                li.innerText = element;
            }
        })
    }
}

arrayToList(array1);
arrayToList(array2)